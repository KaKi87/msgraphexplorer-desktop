document.addEventListener(
    'DOMContentLoaded',
    () => {
        document.body.setAttribute('data-set-preferred-mode-onload', 'true');
        const scriptElement = document.createElement('script');
        scriptElement.setAttribute('src', 'https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/js/halfmoon.min.js');
        scriptElement.addEventListener(
            'load',
            () => window.halfmoonOnDOMContentLoaded()
        );
        document.head.appendChild(scriptElement);
    }
);