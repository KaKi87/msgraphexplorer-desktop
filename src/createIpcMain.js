export default ({
    ipcMain,
    BrowserWindow,
    config
}) => {
    ipcMain.handle(
        'getCredentials',
        () => ({
            clientId: config.get('clientId'),
            clientSecret: config.get('clientSecret'),
            redirectUri: config.get('redirectUri')
        })
    );
    ipcMain.handle(
        'popupReady',
        event => {
            const popupWindow = BrowserWindow.fromWebContents(event.sender);
            popupWindow.setMenu(null);
            popupWindow.setMenuBarVisibility(false);
            popupWindow.show();
        }
    );
};