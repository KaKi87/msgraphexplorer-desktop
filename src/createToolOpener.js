import path from 'node:path';

const __dirname = new URL('.', import.meta.url).pathname;

export default ({
    BrowserWindow,
    mainWindow
}) => async toolId => {
    const toolWindow = new BrowserWindow({
        show: false,
        parent: mainWindow,
        webPreferences: {
            preload: path.join(__dirname, `../assets/tools/preload.cjs`)
        }
    });
    toolWindow.setMenu(null);
    toolWindow.setMenuBarVisibility(false);
    await toolWindow.loadFile(path.join(__dirname, `../assets/tools/${toolId}.html`));
    await toolWindow.show();
};