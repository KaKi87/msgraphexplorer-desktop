const { ipcRenderer } = require('electron');

const defaultStyleElement = document.createElement('style');
defaultStyleElement.appendChild(document.createTextNode(`
    @media (prefers-color-scheme: dark) {
        html {
            color-scheme: dark;
            filter: invert(1) hue-rotate(180deg) contrast(.7) !important;
            background: #FFF;
        }
    }
`));

const oauthStyleElement = document.createElement('style');
oauthStyleElement.appendChild(document.createTextNode(`
    body > *:not(.MSGRAPHEXPLORER-DESKTOP){
        display: none;
    }
    .MSGRAPHEXPLORER-DESKTOP {
        position: absolute;
        top: 0;
        left: 0;
        width: 100vw;
        height: 100vh;
        overflow: auto;
    }
    .MSGRAPHEXPLORER-DESKTOP__form {
        padding: 1rem;
        display: flex;
        flex-direction: column;
        row-gap: 1rem;
        font-family: sans-serif;
    }
    .MSGRAPHEXPLORER-DESKTOP__form__input {
        width: 100%;
        font-family: monospace;
    }
`));

const oauthContainerElement = document.createElement('div');
oauthContainerElement.classList.add('MSGRAPHEXPLORER-DESKTOP');
oauthContainerElement.innerHTML = `
    <form class="MSGRAPHEXPLORER-DESKTOP__form">
        <label>
            Client ID
            <br>
            <input class="MSGRAPHEXPLORER-DESKTOP__form__input MSGRAPHEXPLORER-DESKTOP__form__input--clientId" type="text">
        </label>
        <label>
            Client secret
            <br>
            <input class="MSGRAPHEXPLORER-DESKTOP__form__input MSGRAPHEXPLORER-DESKTOP__form__input--clientSecret" type="text">
        </label>
        <label>
            Redirect URI
            <br>
            <input class="MSGRAPHEXPLORER-DESKTOP__form__input MSGRAPHEXPLORER-DESKTOP__form__input--redirectUri" type="text">
        </label>
        <label>
            Access token
            <br>
            <input class="MSGRAPHEXPLORER-DESKTOP__form__input MSGRAPHEXPLORER-DESKTOP__form__input--accessToken" type="text">
        </label>
        <label>
            Refresh token
            <br>
            <input class="MSGRAPHEXPLORER-DESKTOP__form__input MSGRAPHEXPLORER-DESKTOP__form__input--refreshToken" type="text">
        </label>
        <input type="submit">
    </form>
`;

window.addEventListener(
    'DOMContentLoaded',
    async () => {
        document.head.appendChild(defaultStyleElement);
        const {
            'redirect_uri': oauthRedirectUri,
            'state': oauthState
        } = Object.fromEntries(new URL(window.location.href).searchParams.entries());
        if(oauthRedirectUri && oauthState){
            const credentials = await ipcRenderer.invoke('getCredentials');
            document.head.appendChild(oauthStyleElement);
            document.body.appendChild(oauthContainerElement);
            Object.entries(credentials).forEach(([key, value]) => document.querySelector(`.MSGRAPHEXPLORER-DESKTOP__form__input--${key}`).value = value || '');
            document.querySelector('.MSGRAPHEXPLORER-DESKTOP__form').addEventListener(
                'submit',
                event => {
                    event.preventDefault();
                    const
                        [
                            clientId,
                            clientSecret,
                            redirectUri,
                            accessToken,
                            refreshToken
                        ] = [
                            'clientId',
                            'clientSecret',
                            'redirectUri',
                            'accessToken',
                            'refreshToken'
                        ].map(key => document.querySelector(`.MSGRAPHEXPLORER-DESKTOP__form__input--${key}`).value),
                        params = new URLSearchParams();
                    params.set('code', btoa(JSON.stringify({
                        clientId,
                        clientSecret,
                        redirectUri,
                        accessToken,
                        refreshToken
                    })));
                    params.set('state', oauthState);
                    window.location.assign(`${oauthRedirectUri}#${params}`);
                }
            );
            await ipcRenderer.invoke('popupReady');
        }
        else if(window.location.origin === 'https://jwt.ms')
            await ipcRenderer.invoke('popupReady');
        else if(window.location.href === 'https://login.microsoftonline.com/common/oauth2/v2.0/logoutsession')
            window.close();
    }
);