import jwt from 'jsonwebtoken';
import axios from 'axios';

export default ({
    session,
    config
}) => {
    let nonce;
    session.defaultSession.protocol.interceptBufferProtocol(
        'https',
        async (
            {
                url,
                method,
                uploadData: [{ bytes: requestPayload } = {}] = [],
                headers: requestHeaders
            },
            callback
        ) => {
            let response;
            if(url.startsWith('https://login.microsoftonline.com/common/oauth2/v2.0/authorize'))
                nonce = new URL(url).searchParams.get('nonce')
            if(url === 'https://login.microsoftonline.com/common/oauth2/v2.0/token'){
                const data = new URLSearchParams(requestPayload.toString());
                switch(data.get('grant_type')){
                    case 'authorization_code': {
                        const
                            code = data.get('code'),
                            {
                                clientId,
                                clientSecret,
                                redirectUri,
                                accessToken,
                                refreshToken
                            } = JSON.parse(Buffer.from(code, 'base64').toString()),
                            decodedAccessToken = jwt.decode(accessToken),
                            accessTokenValidity = decodedAccessToken['exp'] - Math.floor(Date.now() / 1000);
                        config.set({
                            clientId,
                            clientSecret,
                            redirectUri
                        });
                        response = {
                            statusCode: 200,
                            data: new TextEncoder().encode(JSON.stringify({
                                'token_type': 'Bearer',
                                'scope': decodedAccessToken['scp'],
                                'expires_in': accessTokenValidity,
                                'ext_expires_in': accessTokenValidity,
                                'access_token': accessToken,
                                'refresh_token': refreshToken,
                                'id_token': jwt.sign({
                                    'aud': decodedAccessToken['appId'],
                                    'iss': `https://login.microsoftonline.com/${decodedAccessToken['tid']}/v2.0`,
                                    'iat': decodedAccessToken['iat'],
                                    'nbf': decodedAccessToken['nbf'],
                                    'exp': decodedAccessToken['exp'],
                                    'aio': '',
                                    'login_hint': '',
                                    'name': decodedAccessToken['name'],
                                    nonce,
                                    'oid': decodedAccessToken['oid'],
                                    'preferred_username': decodedAccessToken['upn'],
                                    'rh': '',
                                    'sid': '',
                                    'sub': decodedAccessToken['xms_st']['sub'],
                                    'tid': decodedAccessToken['tid'],
                                    'uti': decodedAccessToken['uti'],
                                    'ver': '2.0'
                                }, 'msgraphexplorer-desktop'),
                                'client_info': 'eyJ1aWQiOiJiODQ5ZjUxOS02NzkzLTRmZjgtOTNlYy0wMjE4NTcyZDk0NGUiLCJ1dGlkIjoiMmJlZDc0NmItMTE4Yi00NDEzLWEyNGMtMjYyNTBkNTEwZGJlIn0'
                            }))
                        };
                        break;
                    }
                    case 'refresh_token': {
                        const
                            clientId = config.get('clientId'),
                            clientSecret = config.get('clientSecret'),
                            redirectUri = config.get('redirectUri');
                        requestPayload = data;
                        requestPayload.set('client_id', clientId);
                        requestPayload.set('client_secret', clientSecret);
                        requestPayload.set('redirect_uri', redirectUri);
                        break;
                    }
                }
            }
            if(!response){
                const {
                    status: statusCode,
                    headers: responseHeaders,
                    data: responsePayload
                } = await axios({
                    url,
                    method,
                    data: requestPayload,
                    headers: requestHeaders,
                    responseType: 'arraybuffer',
                    validateStatus: () => true
                });
                response = {
                    statusCode,
                    headers: responseHeaders,
                    data: responsePayload
                };
            }
            callback(response);
        }
    );
};