import Config from 'conf';

export default () => new Config({
    schema: {
        clientId: {
            type: 'string'
        },
        clientSecret: {
            type: 'string'
        },
        redirectUri: {
            type: 'string'
        }
    }
});