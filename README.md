# Microsoft Graph Explorer Desktop

Electron app allowing token-based usage of [Microsoft Graph Explorer](https://developer.microsoft.com/en-us/graph/graph-explorer).

| ![](https://i.goopics.net/y1nkh8.png) | ![](https://i.goopics.net/aq9quq.png) |
|---------------------------------------|---------------------------------------|

## Features

- Manual authentication using access token
- Automated refresh using client ID & secret, redirect URI and refresh token
- Embedded tools
  - ID converter (visio conference ID, thread ID, meeting ID)
- Enhanced styling
  - Useless components hidden (header & footer, including cookies & ToS banners)
  - Dark theme support added for scrollbar, background and OAuth2 popup