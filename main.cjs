const electron = require('electron');

if(typeof electron === 'string') require('node:child_process').spawn(
    electron,
    [__filename],
    { stdio: 'inherit' }
);

else (async () => {
    const
        {
            nativeTheme,
            app,
            ipcMain,
            BrowserWindow,
            session,
            Menu
        } = electron,
        path = require('node:path'),
        fs = require('node:fs/promises'),
        createConfig = (await import('./src/createConfig.js')).default,
        createIpcMain = (await import('./src/createIpcMain.js')).default,
        createInterceptor = (await import('./src/createInterceptor.js')).default,
        createToolOpener = (await import('./src/createToolOpener.js')).default;
    nativeTheme.themeSource = 'dark';
    await app.whenReady();
    const config = createConfig();
    createIpcMain({
        ipcMain,
        BrowserWindow,
        config
    });
    createInterceptor({
        session,
        config
    });
    const
        mainWindow = new BrowserWindow({
            show: false
        }),
        openTool = createToolOpener({
            BrowserWindow,
            mainWindow
        });
    if(!app.isPackaged)
        mainWindow.webContents.openDevTools();
    mainWindow.webContents.setWindowOpenHandler(() => ({
        action: 'allow',
        overrideBrowserWindowOptions: {
            show: false,
            webPreferences: {
                preload: path.join(__dirname, './src/popupPreload.js')
            }
        }
    }));
    await mainWindow.loadURL('https://developer.microsoft.com/en-us/graph/graph-explorer');
    await mainWindow.webContents.insertCSS(await fs.readFile(path.join(__dirname, './assets/main.user.css'), 'utf8'));
    mainWindow.setMenu(Menu.buildFromTemplate([
        {
            label: 'Tools',
            submenu: [
                {
                    label: 'ID converter',
                    click: () => openTool('IdConverter')
                }
            ]
        }
    ]));
    mainWindow.show();
})().catch(console.error);